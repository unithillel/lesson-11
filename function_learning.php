<?php

$firstName = "John";
$lastNameOfUser = 'Doe';
$names = ['John', 'Ann', 'Susan'];

function sayHello($name, $lastName = NULL){
    $resultString = "Hello, " . $name;
    if($lastName){
        $resultString.= ' ' . $lastName;
    }
    $resultString .= '! <br>';
    $resultString .="Glad to see you!";
    $resultString .= '<br>';
    return $resultString;
}

function makeSquare($number){
    return round($number * $number, 2);
}

echo sayHello('Bob', 'Robbinson');
echo sayHello($firstName, $lastNameOfUser);
foreach($names as $name){
    echo sayHello($name);
}

$variable = makeSquare(2);
var_dump($variable);
