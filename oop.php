<?php
class Product{
    public $title = "Book name";
    public $authorFirstName = "Author name";
    public $authorLastName = "Author last name";
    public $price = 0;

    public function __construct($title, $authorFirstName, $authorLastName, $price){
        $this->title = $title;
        $this->authorFirstName = $authorFirstName;
        $this->authorLastName = $authorLastName;
        $this->price = $price;
    }

    public function getAuthorFullName(){
        //объект созданный в будущем на основании класса Product
        return $this->authorFirstName . ' ' . $this->authorLastName;
    }

    public function convertPrice($moneyValue){
        return round($this->price * $moneyValue, 2);
    }
};

$item1 = new Product('Php is awesome', 'John', 'Doe', 12);
// $item1->__construct('Php is awesome', 'John', 'Doe', 12);
$item2 = new Product('Javascript is also cool', 'Bob', 'Robbinson', 11);



echo '<br>';
echo $item1->getAuthorFullName();
echo '<br>';
echo $item2->getAuthorFullName();
echo '<br>';
echo $item2->convertPrice(27.7);

die();
