<?php
class AudioProduct extends Product{
    public $playLength = 0;
    public function __construct($title, $authorFirstName, $authorLastName, $price, $playLength){
        parent::__construct($title, $authorFirstName, $authorLastName, $price);
        $this->playLength = $playLength;
    }
    
    public function getSummaryLine(){
        return parent::getSummaryLine() . ' ' .$this->playLength;
    }
}