<?php
class Product{
    public $title = "Book name";
    public $authorFirstName = "Author name";
    public $authorLastName = "Author last name";
    public $price = 0;
    const USD_VALUE = 27.6;

    public function __construct($title, $authorFirstName, $authorLastName, $price){
        $this->title = $title;
        $this->authorFirstName = $authorFirstName;
        $this->authorLastName = $authorLastName;
        $this->price = $price;
    }

    public function getAuthorFullName(){
        //объект созданный в будущем на основании класса Product
        return $this->authorFirstName . ' ' . $this->authorLastName;
    }

    public function convertPrice(){
        return round($this->price *  self::USD_VALUE, 2);
    }

    public function getSummaryLine(){
        return $this->title 
            . ' ' . $this->getAuthorFullName()
            . ', price:' . $this->convertPrice();
    }
};