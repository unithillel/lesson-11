<?php
class BookProduct extends Product{
    public $pages = 0;

    public function __construct($title, $authorFirstName, $authorLastName, $price, $pages){
        parent::__construct($title, $authorFirstName, $authorLastName, $price);
        $this->pages = $pages;
    }

    public function getSummaryLine(){
        return parent::getSummaryLine() . ' ' . $this->pages;
    }

};