<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/Product.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/AudioProduct.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/BookProduct.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/HtmlBookWriter.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/data/products.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/functions/template.php';

$productObjects = [];
foreach($products as $productArray){
    if( $productArray['type'] === 'book'){
        $productObjects[] = new BookProduct($productArray['title'], 
            $productArray['authorFirstName'],
            $productArray['authorLastName'],
            $productArray['price'],
            $productArray['pages'],
        );
    }elseif($productArray['type'] === 'audiobook'){
        $productObjects[] = new AudioProduct($productArray['title'], 
            $productArray['authorFirstName'],
            $productArray['authorLastName'],
            $productArray['price'],
            $productArray['playlength'],
        );
    }
}


template('shop/index', [
    'seoTitle' => 'Modern Book shop',
    'seoDescription' => 'Modern Book shop in Odessa',
    'productsObjects' => $productObjects
]); 
?>