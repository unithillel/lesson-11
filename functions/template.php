<?php
// header

// content

// footer

function template($templateName, $data = []){
    extract($data); 
    if(empty($seoTitle)){
        $seoTitle = "Hillel lesson 11 website";
    }
    if(empty($seoDescription)){
        $seoTitle = "Hillel lesson 11 website Description";
    }

    include $_SERVER['DOCUMENT_ROOT'].'/templates/basic/header.php';

    include $_SERVER['DOCUMENT_ROOT'].'/templates/'.$templateName.'.php';

    include $_SERVER['DOCUMENT_ROOT'].'/templates/basic/footer.php';
}