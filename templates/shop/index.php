<?php
    $writer = new HtmlBookWriter();
    class FakeBook{};
    $fakeBook = new FakeBook;
?>
<div class="content">
    <div class="row d-flex justify-content-center">
        <div class="col-6 ">
            <h1>Most popular books</h1>
            <p>Current usd course: <?=Product::USD_VALUE?></p>
            <ul class="list-group">
                <?php foreach($productsObjects as $product):?>
                    <li class="list-group-item">
                        <?=$product->getSummaryLine()?>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
    <div class="row">
        <h2>All books</h2>
        <?php foreach($productsObjects as $product):?>
            <?=$writer->write($product)?>
        <?php endforeach;?>
        <?=$writer->write($fakeBook)?>
    </div>
</div>