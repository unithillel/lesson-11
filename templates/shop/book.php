<div class="card col-4">
  <div class="card-body">
    <h5 class="card-title"><?=$book->title?></h5>
    <p class="card-text"><?=$book->getAuthorFullName()?></p>
    <p class="card-text"><?=$book->price?></p>
    <a href="/" class="btn btn-primary">More details</a>
  </div>
</div>