<?php 
$products = [
    [
        'title' => 'Php is awesome',
        'authorFirstName' => 'John',
        'authorLastName' => 'Doe',
        'price' => 20,
        'type' => 'book',
        'pages' => 320
    ],
    [
        'title' => 'Javascript is awesome',
        'authorFirstName' => 'John',
        'authorLastName' => 'Doe',
        'price' => 12,
        'type' => 'audiobook',
        'playlength' => 20
    ],
    [
        'title' => 'Html is awesome',
        'authorFirstName' => 'Bob',
        'authorLastName' => 'Doe',
        'price' => 20,
        'type' => 'book',
        'pages' => 320
    ],
    [
        'title' => 'CSS is awesome',
        'authorFirstName' => 'Bob',
        'authorLastName' => 'Doe',
        'price' => 20,
        'type' => 'book',
        'pages' => 320
    ],
    [
        'title' => 'OOP is awesome',
        'authorFirstName' => 'John',
        'authorLastName' => 'Doe',
        'price' => 12,
        'type' => 'audiobook',
        'playlength' => 20
    ],
];